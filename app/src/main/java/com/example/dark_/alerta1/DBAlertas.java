package com.example.dark_.alerta1;

public class DBAlertas {
    String idalerta;
    String titulo_alerta;
    String contenido_alerta;
    String nombre_completo_emitio;
    String cargo_emitio;
    String fecha_alerta;
    String tipo_alerta;
    String estatus_alerta;
    int imagen_notificacion;
    private boolean notificado;

    public DBAlertas() {
        this.idalerta = idalerta;
        this.titulo_alerta = titulo_alerta;
        this.contenido_alerta = contenido_alerta;
        this.nombre_completo_emitio = nombre_completo_emitio;
        this.cargo_emitio = cargo_emitio;
        this.fecha_alerta = fecha_alerta;
        this.tipo_alerta = tipo_alerta;
        this.estatus_alerta = estatus_alerta;
        this.imagen_notificacion=imagen_notificacion;
        this.notificado=false;
    }

    public boolean isNotificado() {
        return notificado;
    }

    public void setNotificado(boolean notificado) {
        this.notificado = notificado;
    }

    public DBAlertas(String idalerta, String titulo_alerta, String contenido_alerta, String nombre_completo_emitio, String cargo_emitio, String fecha_alerta, String tipo_alerta, String estatus_alerta) {
        this.idalerta = idalerta;
        this.titulo_alerta = titulo_alerta;
        this.contenido_alerta = contenido_alerta;
        this.nombre_completo_emitio = nombre_completo_emitio;
        this.cargo_emitio = cargo_emitio;
        this.fecha_alerta = fecha_alerta;
        this.tipo_alerta = tipo_alerta;
        this.estatus_alerta = estatus_alerta;
        this.imagen_notificacion=imagen_notificacion;
        this.notificado = false;
    }



    public String getIdalerta() {
        return idalerta;
    }

    public void setIdalerta(String idalerta) {
        this.idalerta = idalerta;
    }

    public String getTitulo_alerta() {
        return titulo_alerta;
    }

    public void setTitulo_alerta(String titulo_alerta) {
        this.titulo_alerta = titulo_alerta;
    }

    public String getContenido_alerta() {
        return contenido_alerta;
    }

    public void setContenido_alerta(String contenido_alerta) {
        this.contenido_alerta = contenido_alerta;
    }

    public String getNombre_completo_emitio() {
        return nombre_completo_emitio;
    }

    public void setNombre_completo_emitio(String nombre_completo_emitio) {
        this.nombre_completo_emitio = nombre_completo_emitio;
    }

    public String getCargo_emitio() {
        return cargo_emitio;
    }

    public void setCargo_emitio(String cargo_emitio) {
        this.cargo_emitio = cargo_emitio;
    }

    public String getFecha_alerta() {
        return fecha_alerta;
    }

    public void setFecha_alerta(String fecha_alerta) {
        this.fecha_alerta = fecha_alerta;
    }

    public String getTipo_alerta() {
        return tipo_alerta;
    }

    public void setTipo_alerta(String tipo_alerta) {
        this.tipo_alerta = tipo_alerta;
    }

    public String getEstatus_alerta() {
        return estatus_alerta;
    }

    public void setEstatus_alerta(String estatus_alerta) {
        this.estatus_alerta = estatus_alerta;
    }


    public void setImagen_notificacion(int imagen_notificacion) {
        this.imagen_notificacion = imagen_notificacion;
    }

    public int getImagen_notificacion() {
        return imagen_notificacion;
    }
}
