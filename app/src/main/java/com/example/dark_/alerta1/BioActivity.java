package com.example.dark_.alerta1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

public class BioActivity extends AppCompatActivity {
    ArrayList<DBAlertas> ListaNotificaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String titulo_alerta="";
        String fecha_alerta="";
        String contenido_alerta="";
        String nombre_completo_emitio="";
        String cargo_emitio="";

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
             titulo_alerta=extras.getString("titulo_alerta");
            fecha_alerta=extras.getString("fecha_alerta");
            contenido_alerta=extras.getString("contenido_alerta");
            nombre_completo_emitio= extras.getString("nombre_completo_emitio");
            cargo_emitio=extras.getString("cargo_emitio");
        }
        TextView idtitulo_alerta_detalle=(TextView)findViewById(R.id.idtitulo_alerta_detalle);
        idtitulo_alerta_detalle.setText(titulo_alerta);

       TextView idfecha_alerta_detalle=(TextView)findViewById(R.id.idfecha_alerta_detalle);
        idfecha_alerta_detalle.setText(fecha_alerta);

        TextView idcontenido_alerta_detalle=(TextView)findViewById(R.id.idcontenido_alerta_detalle);
        idcontenido_alerta_detalle.setText(contenido_alerta);

        TextView idnombre_completo_emitio_detalle=(TextView)findViewById(R.id.idnombre_completo_emitio_detalle);
        idnombre_completo_emitio_detalle.setText(nombre_completo_emitio);

        TextView idcargo_emitio_detalle=(TextView)findViewById(R.id.idcargo_emitio_detalle);
        idcargo_emitio_detalle.setText(cargo_emitio);




    }


}

