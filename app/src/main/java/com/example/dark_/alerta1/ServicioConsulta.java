package com.example.dark_.alerta1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ServicioConsulta extends Service {
    private static final String TAG = "MyServiceConsulta";
    private ArrayList<DBAlertas> ListaNotificaciones;
    private String urll;
    private int ultimoID;
    String tipoavisooo;
    Date date;
    Adaptador adaptador;

    Handler handler;
    Runnable runnable;

    @Override
    public IBinder onBind(Intent i) {

        return null;
    }

    @Override
    public void onCreate() {
        ListaNotificaciones=new ArrayList<>();
        ultimoID = 0;
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {

                llenarnotificaciones();
                handler.postDelayed(runnable, 5000);
            }
        };

        handler.postDelayed(runnable, 5000);
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG, "FirstService destroyed");
        Toast.makeText(getApplicationContext(), "Servicio Termino", Toast.LENGTH_SHORT).show();
    }

    public void showNotification(Context context) {
        Intent i = new Intent(context,MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context, 0, i, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("vida y salud")
                .setContentText("No olvides hacer tu examen mensual");
        mBuilder.setContentIntent(pi);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    public void llenarnotificaciones() {

        adaptador=new Adaptador(ListaNotificaciones);
        urll ="http://www.utvm.edu.mx/webservice/consultaTodosAvisos.php";
        AQuery aq = new AQuery(getApplicationContext());

        aq.progress(1).ajax(urll, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);


                try {
                    JSONArray notificacion = object.getJSONArray("notificacion");

                    for (int i = 0; i < notificacion.length(); i++){

                        JSONObject datos = notificacion.getJSONObject(i);
                        DBAlertas alertas = new DBAlertas();

                        String idalerta = datos.getString("idalerta");
                        String titulo_alerta = datos.getString("titulo_alerta");
                        String contenido_alerta = datos.getString("contenido_alerta");
                        String nombre_completo_emitio = datos.getString("nombre_completo_emitio");
                        String cargo_emitio = datos.getString("cargo_emitio");
                        String fecha_alerta = datos.getString("fecha_alerta");
                        String tipo_alerta = datos.getString("tipo_alerta");
                        String estatus_alerta = datos.getString("estatus_alerta");

                        if(tipo_alerta.equals("1")){
                            alertas.setImagen_notificacion(R.drawable.sirena1);
                        }
                        else if(tipo_alerta.equals("2")){
                            alertas.setImagen_notificacion(R.drawable.alert2);
                        }
                        else{
                            alertas.setImagen_notificacion(R.drawable.logo);
                        }
                        alertas.setIdalerta(idalerta);
                        alertas.setTitulo_alerta(titulo_alerta);
                        alertas.setContenido_alerta(contenido_alerta);
                        alertas.setNombre_completo_emitio(nombre_completo_emitio);
                        alertas.setCargo_emitio(cargo_emitio);
                        alertas.setFecha_alerta(fecha_alerta);
                        alertas.setTipo_alerta(tipo_alerta);
                        alertas.setEstatus_alerta(estatus_alerta);
                        ListaNotificaciones.add(alertas);




                        adaptador.notifyDataSetChanged();

                        String [] fecha_hora = fecha_alerta.split(" ");
                        String[] datos_fecha = fecha_hora[0].split("/");
                        int anioN = Integer.parseInt(datos_fecha[2]);
                        int mesN = Integer.parseInt(datos_fecha[1]);
                        int diaN = Integer.parseInt(datos_fecha[0]);
                        String[] datos_hora = fecha_hora[1].split(":");
                        int horaN = Integer.parseInt(datos_hora[0]);
                        int minutoN = Integer.parseInt(datos_hora[1]);
                        int segundoN = Integer.parseInt(datos_hora[2]);
                        Calendar fecha = Calendar.getInstance();
                        int anio = fecha.get(Calendar.YEAR);
                        int mes = fecha.get(Calendar.MONTH) + 1;
                        int dia = fecha.get(Calendar.DAY_OF_MONTH);
                        int hora = fecha.get(Calendar.HOUR_OF_DAY);
                        int minuto = fecha.get(Calendar.MINUTE);
                        int segundo = fecha.get(Calendar.SECOND);
                        if (anio == anioN && dia == diaN && mes == mesN && ultimoID < Integer.parseInt(idalerta) ){
                            if(hora == horaN && (minuto - minutoN) <= 4  ){
                                ultimoID = Integer.parseInt(idalerta);
                                if(tipo_alerta.equals("1")){
                                    activarServicio();
                                }
                                else if(tipo_alerta.equals("2")){
                                    activarServicio2();
                                }
                                
                            }
                        }



                    }

                }
                catch (JSONException e){
                    mostrarmensaje("Error de Conexión, por favor intente más tarde.\n Gracias");
                    Log.e("json erorr",e.getLocalizedMessage());
                }
            }
        });
    }

    public void mostrarmensaje(String mensaje){
        Toast toast=Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_SHORT);
        toast.show();
    }
    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);

    }

    public void activarServicio() {
        String textoalerta = "AVISO UTVM !!";
        String textotituloalerta = "Aviso de la intranet !! ";
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Uri uri = Uri.parse(String.format("android.resource://%s/%s/%s", getApplication().getPackageName(), "raw", "alertasismicacdmx"));


        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getBaseContext())
                .setSmallIcon(R.drawable.alert3)
                .setContentTitle(textotituloalerta)
                .setContentText(textoalerta)
                .setLights(Color.RED,3000,3000)
                //.setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setSound(uri)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setVibrate(new long[]{100, 5000, 100, 5000, 500, 5000, 900, 5000, 500, 2000})
                .setAutoCancel(true);


        Intent notificacionIntent = new Intent(getBaseContext(), MainActivity.class);
        PendingIntent Intent1 = PendingIntent.getActivity(
                getBaseContext(), 0, notificacionIntent,
                PendingIntent.FLAG_ONE_SHOT);


        builder.setContentIntent(Intent1);

        nManager.notify(12345, builder.build());




    }

    public void activarServicio2() {
        String textoalerta = "AVISO UTVM !!";
        String textotituloalerta = "Aviso de la intranet !! ";
        // Instanciamos e inicializamos nuestro manager.
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Uri uri = Uri.parse(String.format("android.resource://%s/%s/%s", getApplication().getPackageName(), "raw", "avisoutvm"));


        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getBaseContext())
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(textotituloalerta)
                .setContentText(textoalerta)
                //.setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setSound(uri)
                .setVibrate(new long[]{100, 5000, 100, 5000})
                .setAutoCancel(true);

        Intent notificacionIntent = new Intent(getBaseContext(), MainActivity.class);
        PendingIntent Intent1 = PendingIntent.getActivity(
                getBaseContext(), 0, notificacionIntent,
                PendingIntent.FLAG_ONE_SHOT);


        builder.setContentIntent(Intent1);
        nManager.notify(123456, builder.build());

    }
    public Date convertir(String horajson){
        String time = horajson;
        SimpleDateFormat dateFormat = new SimpleDateFormat("hmmaa");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = dateFormat.parse(time);

            String out = dateFormat2.format(date);
            Log.e("fecha", out);

            return date;
        } catch (ParseException e) {
        }
        return null;
    }




}

