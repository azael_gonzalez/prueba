package com.example.dark_.alerta1;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.androidquery.util.AQUtility.getContext;

/**
 * Created by dark_ on 17/04/2018.
 */

public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolderDatos> {
    private ArrayList<DBAlertas> ListaNotificaciones;

    SqlDataHelper conn;

    public Adaptador(ArrayList<DBAlertas> ListaNotificaciones) {
        this.ListaNotificaciones = ListaNotificaciones;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notificacion, null, false);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int position) {

        holder.titulo_alerta.setText(ListaNotificaciones.get(position).getTitulo_alerta());
        holder.fecha_alerta.setText(ListaNotificaciones.get(position).getFecha_alerta());
        holder.contenido_alerta.setText(ListaNotificaciones.get(position).getContenido_alerta());
        holder.nombre_completo_emitio.setText(ListaNotificaciones.get(position).getNombre_completo_emitio());
        holder.cargo_emitio.setText(ListaNotificaciones.get(position).getCargo_emitio());

        holder.imagen_notificacion.setImageResource(ListaNotificaciones.get(position).getImagen_notificacion());

        //Evento Boton
        holder.setOnClickListeners();


    }

    @Override
    public int getItemCount() {
        return ListaNotificaciones.size();
    }


    public class ViewHolderDatos extends RecyclerView.ViewHolder implements View.OnClickListener {
        //context
        Context context;

        CardView item_notificacion;
        TextView titulo_alerta, fecha_alerta,contenido_alerta,nombre_completo_emitio,cargo_emitio;
        ImageView imagen_notificacion;
        Button btnDetails;

        public ViewHolderDatos(View itemView) {
            super(itemView);

            context = itemView.getContext();
            //elementos vista general
            item_notificacion = (CardView) itemView.findViewById(R.id.item_notificacion);
            titulo_alerta = (TextView) itemView.findViewById(R.id.idtitulo_alerta);
            fecha_alerta = (TextView) itemView.findViewById(R.id.idfecha_alerta);

            imagen_notificacion = (ImageView) itemView.findViewById(R.id.idimagen_notificacion);
            btnDetails = (Button) itemView.findViewById(R.id.idbotonVer);
            //elementos vista detallada
            contenido_alerta=(TextView)itemView.findViewById(R.id.id1);
            nombre_completo_emitio=(TextView)itemView.findViewById(R.id.id2);
            cargo_emitio=(TextView)itemView.findViewById(R.id.id3);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, BioActivity.class);
            intent.putExtra("titulo_alerta",titulo_alerta.getText());
            intent.putExtra("fecha_alerta",fecha_alerta.getText());
            intent.putExtra("contenido_alerta",contenido_alerta.getText());
            intent.putExtra("nombre_completo_emitio",nombre_completo_emitio.getText());
            intent.putExtra("cargo_emitio",cargo_emitio.getText());

            context.startActivity(intent);
        }

        public void setOnClickListeners() {
            btnDetails.setOnClickListener(this);
        }
    }
    }





