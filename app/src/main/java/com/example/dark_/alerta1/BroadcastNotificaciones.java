package com.example.dark_.alerta1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by dark_ on 25/04/2018.
 */

public class BroadcastNotificaciones extends BroadcastReceiver {

    private Context context;

    public BroadcastNotificaciones(Context context){
        this.context = context;
    }

    public BroadcastNotificaciones(){

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            // Simulamos trabajo de 1 segundo.


            while (true){
                Thread.sleep(5000);
                showNotification(context);
            }

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void showNotification(Context context) {
        Intent i = new Intent(context,MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context, 0, i, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("vida y salud")
                .setContentText("No olvides hacer tu examen mensual");
        mBuilder.setContentIntent(pi);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }
}