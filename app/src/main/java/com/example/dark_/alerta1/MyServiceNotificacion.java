package com.example.dark_.alerta1;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class MyServiceNotificacion extends Service {
	private static final String TAG = "MyService2";


	String textoalerta = "ALERTA DE SISMO !!";
	String textotituloalerta = "ALERTA !! ";

	@Override
	public IBinder onBind(Intent i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {

		new Thread(new Runnable() {

			@Override
			public void run() {
				Log.d(TAG, "Incio de servicio de notificación");
				// El servicio se finaliza a si mismo cuando finaliza su
				// trabajo.
				try {
					// Simulamos trabajo de 1 segundo.
					Thread.sleep(1000);

					// Instanciamos e inicializamos nuestro manager.
					NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

					Uri uri = Uri.parse(String.format("android.resource://%s/%s/%s", getApplication().getPackageName(), "raw", "alertasismicacdmx"));


					NotificationCompat.Builder builder = new NotificationCompat.Builder(
							getBaseContext())
							.setSmallIcon(R.drawable.logo)
							.setContentTitle(textotituloalerta)
							.setContentText(textoalerta)
							.setLights(Color.RED,3000,3000)
							//.setContentIntent(pendingIntent)
							.setWhen(System.currentTimeMillis())
							.setSound(uri)
							.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
							.setVibrate(new long[]{100, 5000, 100, 5000, 500, 5000, 900, 5000, 500, 2000})
							.setAutoCancel(true);


					PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
					PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Tag");
					wl.acquire(3000);
					wl.release();


					nManager.notify(12345, builder.build());


					Log.d(TAG, "sleep finished");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}).start();

		this.stopSelf();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.d(TAG, "FirstService destroyed");
	}


}

