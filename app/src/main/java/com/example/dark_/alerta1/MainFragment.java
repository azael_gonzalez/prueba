package com.example.dark_.alerta1;

import android.app.Activity;
import android.app.Fragment;
import android.app.Notification;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.stream.JsonReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.androidquery.util.AQUtility.getContext;


public class MainFragment extends Fragment {
    //fragmento lista notificaciones

    SqlDataHelper conn ;
    private ArrayList<DBAlertas> ListaNotificaciones;
    private RecyclerView recyclernotificaciones;
    private String urll;
    Date date;



    private Button btnLinterna;
    private boolean encendida;
   // String sstitulo_alerta;
    View view;
    Adaptador adaptador;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle saved) {
       view = inflater.inflate(R.layout.mainfragment, null);
        conn= new SqlDataHelper(getActivity().getApplicationContext());
        //fragmento lista notificaciones
        recyclernotificaciones=(RecyclerView)view.findViewById(R.id.idRecicler);
        recyclernotificaciones.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

        llenarnotificaciones();

        adaptador=new Adaptador(ListaNotificaciones);
        recyclernotificaciones.setAdapter(adaptador);

        return view;}

    public void llenarnotificaciones() {

        urll ="http://www.utvm.edu.mx/webservice/consultaTodosAvisos.php";
        AQuery aq = new AQuery(view.getContext().getApplicationContext());
        ListaNotificaciones=new ArrayList<>();
        aq.progress(1).ajax(urll, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                try {
                    JSONArray notificacion = object.getJSONArray("notificacion");

                    for (int i = 0; i < notificacion.length(); i++) {
                        JSONObject datos = notificacion.getJSONObject(i);
                        DBAlertas alertas = new DBAlertas();

                        String idalerta = datos.getString("idalerta");
                        String titulo_alerta = datos.getString("titulo_alerta");
                        String contenido_alerta = datos.getString("contenido_alerta");
                        String nombre_completo_emitio = datos.getString("nombre_completo_emitio");
                        String cargo_emitio = datos.getString("cargo_emitio");
                        String fecha_alerta = datos.getString("fecha_alerta");
                        String tipo_alerta = datos.getString("tipo_alerta");
                        String estatus_alerta = datos.getString("estatus_alerta");

                        if (tipo_alerta.equals("1")) {
                            alertas.setImagen_notificacion(R.drawable.sirena1);
                        } else if (tipo_alerta.equals("2")) {
                            alertas.setImagen_notificacion(R.drawable.alert2);
                        } else {
                            alertas.setImagen_notificacion(R.drawable.logo);
                        }
                        alertas.setIdalerta(idalerta);
                        alertas.setTitulo_alerta(titulo_alerta);
                        alertas.setContenido_alerta(contenido_alerta);
                        alertas.setNombre_completo_emitio(nombre_completo_emitio);
                        alertas.setCargo_emitio(cargo_emitio);
                        alertas.setFecha_alerta(fecha_alerta);
                        alertas.setTipo_alerta(tipo_alerta);
                        alertas.setEstatus_alerta(estatus_alerta);
                        ListaNotificaciones.add(alertas);

                        adaptador.notifyDataSetChanged();

///// se quito codigo

                    }
                }
                catch (JSONException e){
                    Toast.makeText(view.getContext(),"Error de Conexión, por favor intente más tarde.\n Gracias",Toast.LENGTH_SHORT).show();
                    Log.e("json erorr",e.getLocalizedMessage());
                }
            }
        });
    }

    public void mostrarmensaje(String mensaje){
        Toast toast=Toast.makeText(getView().getContext(),mensaje,Toast.LENGTH_SHORT);
        toast.show();
    }
    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);

    }

    public void activarServicio() {
        Log.d("MainActivity", "activarServicio()");
        Intent service = new Intent(view.getContext(), MyServiceNotificacion.class);
        view.getContext().startService(service);

    }

    public void activarServicioconsult() {
        Log.d("MainActivity", "activarServicio()");
        Intent service = new Intent(view.getContext(), ServicioConsulta.class);
        view.getContext().startService(service);

    }
    public void activarServicio2() {
        Log.d("MainActivity", "activarServicio()");
        Intent service = new Intent(view.getContext(), ServicioNotificacion.class);
        view.getContext().startService(service);

    }

    public Date convertir(String horajson){
        String time = horajson;
        SimpleDateFormat dateFormat = new SimpleDateFormat("hmmaa");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = dateFormat.parse(time);

            String out = dateFormat2.format(date);
            Log.e("fecha", out);

            return date;
        } catch (ParseException e) {
        }
        return null;
    }

    public void prendeflash(){
        Activity estaActividad = getActivity();
        ((ManejaFlashCamara)estaActividad).enciendeApaga(encendida);

    }
    public void apagaflash(){
        Activity estaActividad = getActivity();
        ((ManejaFlashCamara)estaActividad).enciendeApaga(encendida);

    }
}
