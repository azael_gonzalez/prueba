package com.example.dark_.alerta1;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class ServicioNotificacion extends Service {
    private static final String TAG = "MyService";


    String textoalerta = "AVISO UTVM !!";
    String textotituloalerta = "Aviso de la intranet !! ";

    @Override
    public IBinder onBind(Intent i) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        new Thread(new Runnable() {

            @Override
            public void run() {
                Log.d(TAG, "Incio de servicio de notificación");
                // El servicio se finaliza a si mismo cuando finaliza su
                // trabajo.
                try {
                    // Simulamos trabajo de 1 segundo.
                    Thread.sleep(1000);

                    // Instanciamos e inicializamos nuestro manager.
                    NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    Uri uri = Uri.parse(String.format("android.resource://%s/%s/%s", getApplication().getPackageName(), "raw", "avisoutvm"));


                    NotificationCompat.Builder builder = new NotificationCompat.Builder(
                            getBaseContext())
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle(textotituloalerta)
                            .setContentText(textoalerta)
                            //.setContentIntent(pendingIntent)
                            .setWhen(System.currentTimeMillis())
                            .setSound(uri)
                            .setVibrate(new long[]{100, 5000, 100, 5000})
                            .setAutoCancel(true);


                    nManager.notify(123456, builder.build());


                    Log.d(TAG, "sleep finished");
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }).start();

        this.stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG, "FirstService destroyed");
    }


}

