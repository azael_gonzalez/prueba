package com.example.dark_.alerta1;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

public class ManagerFragment extends Fragment {

    SqlDataHelper dbn;
    Button exportar ;
    String currentDBPath, backupDBPath;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle saved){
        view = inflater.inflate(R.layout.manager, null);

        exportar = (Button)view.findViewById(R.id.exportar);

        //ActionBar actionBar = getSupportActionBar();
        dbn = new SqlDataHelper(view.getContext());

        currentDBPath = "/data/com.example.dark_.menu/databases/contactsManager";
        backupDBPath ="ALERTASUTVM/contactsManager";
        Toast toaste = Toast.makeText(view.getContext(), "Respaldo\n", Toast.LENGTH_SHORT);
        toaste.show();
        activarServicio();

        return view;


    }
    public void activarServicio() {
        Log.d("MainActivity", "activarServicio()");
        Toast toasteinicio = Toast.makeText(view.getContext(), "activarServicio\n", Toast.LENGTH_SHORT);
        toasteinicio.show();

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel src = null;
        FileChannel dst = null;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            src = new FileInputStream(currentDB).getChannel();
            dst = new FileOutputStream(backupDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();

            Toast toast2 = Toast.makeText(view.getContext(), "Respaldo Local realizado\n", Toast.LENGTH_SHORT);
            toast2.show();


            if (currentDB.exists()) {
            }
            if (sd.canWrite()) {
            }
        } catch (Exception e) {
        }
    }

}
