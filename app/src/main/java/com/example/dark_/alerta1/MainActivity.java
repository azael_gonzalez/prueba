package com.example.dark_.alerta1;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.example.dark_.alerta1.R.id.idRecicler;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    SqlDataHelper dbn;
    //frameLayout
    private FrameLayout frameLayout;
    private FragmentManager manager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            dbn = new SqlDataHelper(getApplicationContext());

            //fragmento visualizar uno con vista padre
            frameLayout = (FrameLayout) findViewById(R.id.framefrag);
            manager = getFragmentManager();
            transaction = manager.beginTransaction();
            MainFragment mainFragment = new MainFragment();
            transaction.add(frameLayout.getId(), mainFragment).commit();


            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

        }

        @Override
        public void onBackPressed () {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            // Manejar los clics del elemento de la barra de acción aquí.
            // La barra de acciones gestionará automáticamente
            // los clics en el botón Inicio / Arriba, siempre que
            // especifique una actividad principal en AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                PrefManager prefManager = new PrefManager(getApplicationContext());
                // hacer el lanzamiento por primera vez VERDADERO
                prefManager.setFirstTimeLaunch(true);
                startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
                finish();
                ///boton de configuracion
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected (MenuItem item){
            // Handle navigation view item clicks here.
            int id = item.getItemId();

            manager = getFragmentManager();
            transaction = manager.beginTransaction();

            if (id == R.id.nav_slideshow) {
                //fragmentos
                InstruccionesFragment instruccionesFragment = new InstruccionesFragment();
                transaction.replace(frameLayout.getId(), instruccionesFragment).commit();

            } else if (id == R.id.inicio) {
                MainFragment mainFragment = new MainFragment();
                transaction.replace(frameLayout.getId(), mainFragment).commit();

            } else if (id == R.id.nav_manage) {
                AvisosFragment avisosFragment = new AvisosFragment();
                transaction.replace(frameLayout.getId(), avisosFragment).commit();

            } else if (id == R.id.nav_send) {
                CreditosFragment creditosFragment = new CreditosFragment();
                transaction.replace(frameLayout.getId(), creditosFragment).commit();

            } else if (id == R.id.huevo) {
                ManagerFragment managerFragment = new ManagerFragment();
                transaction.replace(frameLayout.getId(), managerFragment).commit();

            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    public void activarServicio() {
        Log.d("MainActivity", "activarServicio()");
        Intent service = new Intent(this, MyServiceNotificacion.class);
        startService(service);

        }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
       "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


}

